= TP : Build + Tests unitaires + Intégration continue

image::https://gitlab.com/uncoded/GL_TP/badges/master/pipeline.svg[link=https://gitlab.com/uncoded/GL_TP/pipelines?scope=branch]

Le corrigé de ce TP est lui-même un dépôt git.

Chaque étape/exercice fait l'objet d'un commit séparé.
Vous pouvez en voir la liste avec une commande du type `git log`.

Chaque commit à son propre 'hash', c'est à dire son propre identifiant unique.
Donc, vous pouvez voir chacune des étapes du corrigé avec une commande du type
`git checkout <hash>`.

Si vous ne vous souvenez pas du hash qui correspond à un exercice particulier,
vous pouvez toujours revenir au dernier exercice avec une commande du type
`git checkout master`, puis faire un `git log` pour avoir le hash désiré.

**Sujet du TP :** https://uncoded.gitlab.io/GL/fr/unit_testing_python.html

**Résultat :** https://uncoded.gitlab.io/GL_TP
